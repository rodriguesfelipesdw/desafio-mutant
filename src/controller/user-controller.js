const Service = require("../service/serviceClass")
const helpers = require('../helpers/helpers')
const serviceCall = new Service()

module.exports = {
    async getUsers(req,res){
        res.send(await serviceCall.serviceApi())
        
    },

    async getAllWebSites(req, res) {
        let websites = []
        const allUsers = await serviceCall.serviceApi()  
        allUsers.map( user => websites.push(user.website) )
        res.send(websites)
    },

    async getBusinessInfo(req, res) {
        let businessInfo = []
        const allUsers = await serviceCall.serviceApi()  
        allUsers.map( user => {
            const newUser = {
                name: user.name,
                emaail: user.email,
                companyName: user.company.name
            }
            businessInfo.push(newUser)
        })
        businessInfo = helpers.order(businessInfo)
        res.send(businessInfo)
    },

    async getSuiteAdress(req, res) {
        let suiteUsers = []
        const allUsers = await serviceCall.serviceApi()  
        allUsers.map( user => user.address.suite.toLowerCase().includes('suite') ? suiteUsers.push(user) : false )
        res.send(suiteUsers)
    }

}

