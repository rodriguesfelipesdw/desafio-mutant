const axios = require('axios')

class ServiceClass {
    constructor() {
        this._apiHost = `https://jsonplaceholder.typicode.com/users`
    }

    async serviceApi() {
        try {
            const URL = this._apiHost
            const response = await axios.get(URL);
            return response.data 
        } catch (error) {
            throw error
        }
    }
}

module.exports = ServiceClass
