const request = require("supertest")
const app = require("../app")

describe("Test the routes: ", () => {
  test("It should response the GET method for all Users", done => {
    request(app)
      .get("/")
      .then(response => {
        expect(response.statusCode).toBe(200)
        done()
      })
  })
  test("It should response the GET method for Website Users", done => {
    request(app)
      .get("/websites")
      .then(response => {
        expect(response.statusCode).toBe(200)
        done()
      })
  })
  test("It should response the GET method for Info Users", done => {
    request(app)
      .get("/info")
      .then(response => {
        expect(response.statusCode).toBe(200)
        done()
      })
  })
  test("It should response the GET method for Suite Users", done => {
    request(app)
      .get("/suite")
      .then(response => {
        expect(response.statusCode).toBe(200)
        done()
      })
  })
})
