const express = require('express');
const app = express();    
const logger = require('./logger/logger');

const loggerMiddleware = (req, res, next) => {
  try {
    logger.logger(req,res)
    next();
  } catch (error) {
    throw error
  }
}

app.use(loggerMiddleware);
app.use(require('./routes/routes'))
module.exports = app