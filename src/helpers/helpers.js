module.exports.order = arrObj => 
    arrObj.sort( (a,b) => { 
        a = a.name.toLowerCase();
        b = b.name.toLowerCase();
        return a < b ? -1 : a > b ? 1 : 0;
    })
