const express = require('express')
const routes = new express.Router()
const UserController = require('../controller/user-controller')


routes.get('/', UserController.getUsers)
routes.get('/websites', UserController.getAllWebSites)
routes.get('/info', UserController.getBusinessInfo)
routes.get('/suite', UserController.getSuiteAdress)

module.exports = routes