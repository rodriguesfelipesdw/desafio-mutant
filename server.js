const app = require('./src/app')
const server = require('http').Server(app)
require('dotenv/config')

server.listen(process.env.PORT, () => {
    console.log(`API executando com sucesso na porta! ${process.env.PORT}`)
})