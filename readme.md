# Desafio Mutant

![alt text](https://mutantbr.com/wp-content/uploads/2017/05/logo-Mutant-min.png)

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

A API está exposta no link: https://intense-springs-63256.herokuapp.com/
### endpoints
```sh
/ #todos os usuários 
```
```sh
/websites #todos os websites 
```
```sh
/info #nome, email e empresa onde os usuários trabalham.
```
```sh
/suite #todos os usuários que no endereço contém a palavra suite.
```

Criado pipeline de execução de testes a cada push no repositório (bitbucket pipelines) (ci/cd).

Api desenvolvida utilizando: 

  - Nodejs 12
  - ExpressJS Framework
  - Docker

### Installation
A API pode ser exposta localmente, utilizando os seguintes comandos:
*Por padrão, a API é exposta na porta 8080.

```sh
$ docker-compose up
```
ou 
```sh
$ npm install
$ npm run start #production
$ npm run start:dev #development
```

### Logs
A cada chamada no endpoint, o log é apresentado no terminal de execução e também é escrito em um arquivo na raiz do projeto chamado request_logs.txt.
ex. log: #[2020-7-25 0:26:11] GET:/info 200 0.013 ms

